---
all: Tot
events:
- entries:
  - category: floss-events
    content: El 1969, [Ken Thompson](https://ca.wikipedia.org/wiki/Ken_Thompson) i
      [Dennis Ritchie](https://ca.wikipedia.org/wiki/Dennis_Ritchie) comencen a treballar
      en l'[UNIX](http://www.unix.org/what_is_unix/history_timeline.html). Escrit
      inicialment en assemblador, aviat es va reescriure en el [C](https://ca.wikipedia.org/wiki/Llenguatge_C),
      un llenguatge creat per Ritchie i considerat d'alt nivell.
    image:
    - caption: Thompson i Ritchie
      src: /images/Thompson_Ritchie.jpg
      url: https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/
    title: Neix l'UNIX
  year: 1969
- entries:
  - category: floss-events
    content: El 1979, [Bjarne Stroustrup](http://www.stroustrup.com/bio.html) començà
      el desenvolupament del «C amb classes», que més tard esdevindria el [C++](http://www.softwarepreservation.org/projects/c_plus_plus/).
      En la seva opinió, era l'únic llenguatge de l'època que permetia escriure programes
      que alhora eren eficients de temps i elegants.
    image:
    - caption: Bjarne Stroustrup
      src: /images/BjarneStroustrup.jpg
      url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup#/media/File:BjarneStroustrup.jpg
    title: Es va crear el C++
  year: 1979
- entries:
  - category: floss-events
    content: El 1984, [Richard Stallman](https://stallman.org/biographies.html#serious)
      començà el desenvolupament del [GNU](https://www.gnu.org/gnu/about-gnu.html)
      («GNU is Not Unix», GNU no és Unix), un sistema operatiu completament lliure
      basat en l'Unix, que era propietari.
    image:
    - caption: Richard Stallman
      src: /images/richard_stallman.jpg
      url: https://phoneia.com/en/30-years-of-the-gnu-manifesto-written-by-richard-stallman/
    title: L'inici del Programari lliure
  year: 1984
- entries:
  - category: floss-events
    content: El 1991, [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds)
      creà el [nucli Linux](https://ca.wikipedia.org/wiki/Linux_(nucli)) basat en
      el [MINIX](http://www.minix3.org/), una versió de l'Unix escrita per [Andre
      Tanenbaum](http://www.cs.vu.nl/~ast/home/faq.html). L'aparició del Linux ha
      revolucionat la història del programari lliure i ha ajudat a popularitzar-lo.
      Vegeu la [infografia dels 25 anys del desenvolupament del nucli Linux](https://web.archive.org/web/20190628003733im_/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-2016.jpg?itok=DqVqiplt).
    image:
    - caption: Linus Torvalds
      src: /images/linus.jpg
      url: https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html
    title: El nucli Linux
  year: 1991
- entries:
  - category: floss-events
    content: El 1993, començaren a aparèixer les primeres distribucions lliures, basades
      en [GNU i Linux](http://www.aboutlinux.info/2005/11/complete-concise-history-of-gnulinux.html).
      Una [distribució GNU/Linux](http://futurist.se/gldt/) normalment està formada
      pel nucli Linux, les eines i biblioteques GNU, i una col·lecció d'aplicacions.
    image:
    - caption: GNU i Tux
      src: /images/gnulinuxLogo.png
      url: /images/gnulinuxLogo.png
    title: Neixen les primeres distribucions
  year: 1993
- entries:
  - category: floss-events
    content: El 1995, l'empresa noruega Troll Tech creà l'entorn de treball multiplataforma
      [Qt](https://wiki.qt.io/About_Qt), amb el qual es crearia KDE l'any següent.
      Les Qt van ser la base de les tecnologies principals de KDE en aquests 20 anys.
      Apreneu més quant a la [història de les Qt](https://wiki.qt.io/Qt_History).
    image:
    - caption: Logotip de les Qt
      src: /images/qt-logo.png
      url: https://doc.qt.io/qt-5/qtquickcontrols2-gallery-example.html
    title: Es van crear les Qt
  year: 1995
- entries:
  - category: kde-events
    content: El 1996, [Matthias Ettrich](http://www.linuxjournal.com/article/6834)
      anuncià la creació del «Kool Desktop Environment (KDE)» (Entorn d'escriptori
      guai), una interfície gràfica per als sistemes Unix, construït amb les Qt i
      el C++, i dissenyat per a l'usuari final. El nom «KDE» fou un joc de paraules
      amb l'entorn gràfic [CDE](https://en.wikipedia.org/wiki/Common_Desktop_Environment),
      que era propietari en aquella època. Llegiu [l'anunci original](https://www.kde.org/announcements/announcement.php)
      del projecte KDE.
    image:
    - caption: Matthias Ettrich
      src: /images/Matthias.jpg
      url: https://www.linuxjournal.com/article/6834
    title: Es va anunciar KDE
  year: 1996
- entries:
  - category: meetings
    content: El 1997, uns 15 desenvolupadors de KDE es trobaren a Arnsberg, Alemanya,
      per a treballar en el projecte i debatre el seu futur. Aquest esdeveniment s'arribà
      a conèixer com [KDE One](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting)).
    image:
    - caption: Arxiu del Cornelius Schumacher
      src: /images/kde-one-arnsberg.png
    title: Congrés de KDE One
  - category: releases
    content: La versió beta 1 de KDE es va [llançar](https://www.kde.org/announcements/beta1announce.php)
      exactament 12 mesos després de l'anunci del projecte. El text del llançament
      emfatitzava que KDE no era un gestor de finestres, sinó un entorn integrat en
      que el gestor de finestres només n'era una part.
    image:
    - caption: Captura de pantalla del KDE beta 1
      src: /images/KDEBeta1.gif
      url: /images/KDEBeta1.gif
    title: KDE beta 1
  - category: kde-events
    content: El 1997, es va fundar a Tübingen, Alemanya, l'entitat sense ànim de lucre
      [KDE e.V.](https://ev.kde.org/), que representa financerament i legalment la
      comunitat KDE.
    image:
    - caption: Logotip de KDE e.V.
      src: /images/ev_large.png
      url: https://ev.kde.org/images/ev_large.png
    title: Es va fundar KDE e.V.
  year: 1997
- entries:
  - category: kde-events
    content: L'acord de fundació de la [KDE Free Qt Foundation](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php)
      està signat per la KDE e.V. i Trolltech, el llavors propietari de les Qt. La
      Fundació [assegura la disponibilitat permanent](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update)
      de les Qt com a programari lliure.
    image:
    - caption: El Konqi amb Qt al seu cor
      src: /images/kde-qt.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Es va crear la Fundació KDE Free Qt
  - category: releases
    content: KDE llançà la [primera versió estable](https://www.kde.org/announcements/announce-1.0.php)
      del seu entorn gràfic el 1998, destacant un entorn de desenvolupament d'aplicacions,
      les KOM/OpenParts, i una vista prèvia del seu paquet ofimàtic. Vegeu les [captures
      de pantalla del KDE 1.x](https://czechia.kde.org/screenshots/kde1shots.php).
    image:
    - caption: KDE 1
      src: /images/KDE1.png
      url: https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png
    title: Es va llançar KDE 1
  year: 1998
- entries:
  - category: kde-events
    content: L'abril de 1999, es va anunciar un drac com el nou assistent animat per
      al Centre d'ajuda del KDE. Era tan graciós que va substituir la mascota anterior
      del projecte, Kandalf, a partir de la versió 3.x. Vegeu la [captura de pantalla
      del KDE 2](https://czechia.kde.org/screenshots/images/large/kde2b3_5.png) que
      mostren el Konqi i el Kandalf.
    image:
    - caption: Konqi
      src: /images/Konqi.png
      url: https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png
    title: Konqi
  - category: meetings
    content: L'octubre de 1999, tingué lloc la segona reunió de desenvolupadors de
      KDE a Erlangen, Alemanya. Llegiu l'[informe](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting))
      del Congrés KDE Two.
    image:
    - caption: Foto de grup (Arxiu del Cornelius Schumacher)
      src: /images/kde-two-erlangen.jpg
    title: Congrés KDE Two
  year: 1999
- entries:
  - category: releases
    content: A partir de la [versió beta 1 del KDE 2](https://www.kde.org/announcements/announce-1.90.php)
      és possible percebre un canvi de denominació del projecte. Els llançaments fins
      ara al·ludien al projecte com a «Entorn d'escriptori K» (K Desktop Environment),
      van començar a esmentar-lo només com a «Escriptori KDE» (KDE Desktop).
    image:
    - caption: Logotip del KDE 2
      src: /images/KDE2logo.png
      url: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png
    title: «Escriptori KDE»
  - category: meetings
    content: El juliol de 2000, es va fer la tercera reunió (beta) dels desenvolupadors
      de KDE a Trysil, Noruega. [Aquí trobareu](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting))
      què es va fer durant el congrés.
    image:
    - caption: Arxiu del Cornelius Schumacher
      src: /images/kde-three-beta-meeting.jpg
    title: Congrés KDE tres beta
  - category: releases
    content: KDE [llançà](https://kde.org/announcements/1-2-3/2.0/) la seva segona
      versió, presentat com a novetats principals el navegador web i gestor de fitxers
      [Konqueror](https://konqueror.org/); i el paquet ofimàtic [KOffice](https://en.wikipedia.org/wiki/KOffice).
      KDE va reescriure quasi sencer el seu codi en aquesta segona versió. Vegeu les
      [captures de pantalla del KDE 2.0](https://czechia.kde.org/screenshots/kde2shots.php).
    image:
    - caption: KDE 2
      src: /images/kde2.png
      url: https://czechia.kde.org/screenshots/images/large/kde2b3_6.png
    title: Es va llançar KDE 2
  year: 2000
- entries:
  - category: releases
    content: A partir de l'[anunci de llançament](https://www.kde.org/announcements/announce-2.1.2.php)
      de la versió 2.1.2 també va haver-hi un canvi de la nomenclatura. Els anuncis
      comencen a esmentar el KDE com a «Projecte KDE».
    image:
    - caption: Pantalla de presentació del KDE 2.1
      src: /images/splashscreen-2.1.png
      url: https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png
    title: «Projecte KDE»
  - category: kde-events
    content: El març de 2001, es va anunciar la creació del grup de dones de la comunitat.
      El [KDE Women](https://community.kde.org/KDE_Women) va néixer amb l'objectiu
      d'ajudar a augmentar el nombre de dones a les comunitats de programari lliure,
      i al KDE en particular. Vegeu el vídeo [«Highlights of KDE Women»](https://www.youtube.com/watch?v=HTwQ-oGTmGA)
      de l'Akademy 2010.
    image:
    - caption: Katie, l'amiga del Konqi
      src: /images/katie.png
      url: https://www.kde.org/stuff/clipart/katie-221x223.jpg
    title: KDE Women
  year: 2001
- entries:
  - category: meetings
    content: El març de 2002, uns 25 desenvolupadors es trobaren per a la [tercera
      reunió de KDE](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting))
      a Nuremberg, Alemanya. Estava a punt de llançar-se el KDE 3 i calia migrar el
      codi del KDE 2 a la nova biblioteca Qt 3.
    image:
    - caption: Foto de grup del KDE Three
      src: /images/ThreeMeeting.jpg
      url: https://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg
    title: Tercera reunió de KDE
  - category: releases
    content: KDE llançà la seva [tercera versió](https://kde.org/announcements/1-2-3/3.0/),
      mostrant com a novetats importants un entorn de treball d'impressió nou, el
      KDEPrint; la traducció del projecte a 50 idiomes; i un paquet d'aplicacions
      educatives, mantingut pel KDE Edutainment Project. Vegeu les [captures de pantalla
      del KDE 3.0](https://czechia.kde.org/screenshots/kde300shots.php).
    image:
    - caption: KDE 3
      src: /images/kde3.jpg
      url: https://czechia.kde.org/screenshots/images/1152x864/kde300-snapshot2-1152x864.jpg
    title: KDE 3
  - category: kde-events
    content: A l'agost de 2002, va haver-hi una [reunió dels membres del consell de
      KDE e.V.](https://ev.kde.org/reports/2002.php) que fou essencial per a establir
      com funciona l'organització. En aquesta reunió es va decidir, entre d'altres
      coses, que es registraria la marca «KDE» i que els membres nous s'haurien de
      convidar i fer costat per part de dos membres actius de KDE e.V.
    image:
    - caption: Foto de grup (Arxiu del Cornelius Schumacher)
      src: /images/kde-ev-meeting.jpg
    title: Reunió de KDE e.V.
  year: 2002
- entries:
  - category: releases
    content: A la [versió 3.1](https://kde.org/announcements/1-2-3/3.1/), la comunitat
      presentà el KDE amb una aparença nova, un tema nou per als ginys anomenat Keramik,
      i el Crystal com a tema predeterminat per a les icones. Vegeu la [Guia de funcionalitats
      noves del KDE 3.1](https://kde.org/info/3.1/feature_guide_1/).
    image:
    - caption: KDE 3.1
      src: /images/kde31.png
      url: https://czechia.kde.org/screenshots/images/3.1/fullsize/8.png
    title: KDE 3.1
  - category: meetings
    content: A l'agost de 2003, uns 100 col·laboradors de KDE de diversos països es
      trobaren en un castell a la República Txeca. L'esdeveniment es va anomenar [Kastle](https://akademy.kde.org/2003)
      i fou el precursor de l'Akademy, l'esdeveniment que arribaria a ser la reunió
      anual de la comunitat.
    image:
    - caption: Foto de grup del Kastle
      src: /images/Kastle.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg
    title: Kastle
  year: 2003
- entries:
  - category: meetings
    content: A l'agost de 2004, tingué lloc la [primera reunió internacional de la
      comunitat](https://conference2004.kde.org/). L'esdeveniment es va organitzar
      a Ludwigsburg, Alemanya, i va iniciar una sèrie d'esdeveniments internacionals
      anomenats [«Akademy»](https://akademy.kde.org/) que tenen lloc anualment des
      de llavors. L'esdeveniment s'anomenà així perquè se celebrà a l'escola de cinema
      «Filmakademie» de la ciutat. Vegeu les [fotos de grup](http://devel-home.kde.org/~duffus/akademy/)
      de totes les Akademies.
    image:
    - caption: Foto de grup de l'Akademy 2004
      src: /images/akademy-2004.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg
    title: Akademy 2004
  year: 2004
- entries:
  - category: releases
    content: '[Es va llançar el KDE 3.5](https://www.kde.org/announcements/announce-3.5.php).
      Aquesta versió presentà diverses funcionalitats noves, entre elles la SuperKaramba,
      una eina que permetia personalitzar l''escriptori amb «miniaplicacions»; els
      reproductors Amarok i Kaffeine; i el gravador de suports K3B. Vegeu [KDE 3.5:
      Una guia de les funcionalitats noves](https://www.kde.org/announcements/visualguide-3.5.php).'
    image:
    - caption: KDE 3.5
      src: /images/kde35.png
      url: https://czechia.kde.org/screenshots/images/3.5/35-superkaramba.png
    title: KDE 3.5
  year: 2005
- entries:
  - category: meetings
    content: El març de 2006, tingué lloc a Barcelona la [primera reunió](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona)
      de col·laboradors espanyols de KDE. Des de llavors, l'Akademy-Es s'ha tornat
      un esdeveniment anual. Apreneu més quant al [grup de col·laboradors espanyols
      de KDE](https://www.kde-espana.org/).
    image:
    - caption: Foto de la primera Akademy-Es
      src: /images/akademy-es.jpg
      url: https://i.imgur.com/Le0ZAIf.jpg
    title: Primera Akademy-Es
  - category: meetings
    content: El juliol de 2006, els desenvolupadors de les biblioteques del nucli
      de KDE es trobaren a Trysill, Noruega, per a la [quarta reunió del nucli de
      KDE](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core). L'esdeveniment
      fou una espècie de successor del Congrés KDE tres beta i de la Reunió tres de
      KDE, i els desenvolupadors treballaren en el desenvolupament del KDE 4 i l'estabilització
      d'algunes biblioteques del nucli del projecte.
    image:
    - caption: Foto de grup (Arxiu del Cornelius Schumacher)
      src: /images/kde-four-core-meeting.jpg
    title: Quarta reunió del nucli de KDE
  year: 2006
- entries:
  - category: meetings
    content: El març de 2007, diversos col·laboradors de KDE i [Gnome](https://www.gnome.org/)
      es trobaren a A Coruña, Espanya, un esdeveniment que cercava establir una a
      col·laboració entre els dos projectes. L'esdeveniment es conegué com [Guademy](https://dot.kde.org/2007/03/28/guademy-2007-event-report),
      una barreja de [Guadec](https://wiki.gnome.org/GUADEC), el nom que es dóna a
      l'esdeveniment Gnome i Akademy, el nom de l'esdeveniment KDE.
    image:
    - caption: Foto de grup de la Guademy
      src: /images/Guademy.jpg
      url: https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg
    title: Guademy
  - category: releases
    content: 'El maig de 2007, es va anunciar [la versió alfa 1 del KDE 4](https://www.kde.org/announcements/announce-4.0-alpha1.php),
      amb nom en clau «Knut». Aquest anunci mostrà un escriptori completament nou,
      amb el tema Oxygen nou, aplicacions noves com l''Okular i el Dolphin, i una
      nova àrea de treball de l''escriptori, el Plasma. Vegeu [KDE 4.0 alfa 1: Una
      guia visual a les funcionalitats noves](https://www.kde.org/announcements/visualguide-4.0-alpha1.php).'
    image:
    - caption: KDE 4 alfa 1
      src: /images/kde4alpha.png
      url: https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png
    title: KDE 4 alfa 1
  - category: releases
    content: L'octubre de 2007, KDE anuncià la [candidata a llançament](https://www.kde.org/announcements/announce-4.0-platform-rc1.php)
      de la seva plataforma de desenvolupament que consistia en les biblioteques bàsiques
      i les eines per a desenvolupar aplicacions KDE.
    image:
    - caption: El Konqi desenvolupador
      src: /images/konqi-dev.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Plataforma de desenvolupament del KDE 4
  year: 2007
- entries:
  - category: releases
    content: 'En 2008, la comunitat [anuncià el revolucionari KDE 4](https://kde.org/announcements/4/4.0/).
      A més de l''impacte visual del tema predeterminat nou Oxygen, i la nova interfície
      d''escriptori, el Plasma, el KDE 4 també innovà presentant les aplicacions següents:
      el lector de PDF Okular, el gestor de fitxers Dolphin, així com el KWin, que
      permetia efectes gràfics. Vegeu la [Guia visual del KDE 4.0](https://kde.org/announcements/4/4.0/guide/).'
    image:
    - caption: KDE 4.0
      src: /images/kde4.jpg
      url: https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png
    title: KDE 4
  - category: meetings
    content: Des de l'[anunci de la versió 4.1](https://www.kde.org/announcements/4.1/)
      ja hi havia una tendència a referir-se a KDE com una «comunitat» i no només
      un «projecte». Aquest canvi es va reconèixer i afirmar a l'anunci de reconstrucció
      de marca de l'any següent.
    image:
    - caption: Els Konqis comunitat
      src: /images/mascotes.png
      url: https://cibermundi.files.wordpress.com/2016/07/mascotes.png
    title: La comunitat KDE
  year: 2008
- entries:
  - category: meetings
    content: El gener de 2009, tingué lloc a Negril, Jamaica, [la primera edició del
      Camp KDE](https://techbase.kde.org/Events/CampKDE/2009)la primera edició del
      Camp KDE). Fou el primer esdeveniment de KDE a Amèrica. Després d'aquest, hi
      ha hagut dos congressos més situats als EUA, [el 2010 a San Diego](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010),
      i un altre [el 2011 a San Francisco](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience).
    image:
    - caption: Foto de grup del 2009
      src: /images/campkde.jpg
      url: https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg
    title: Primer Camp KDE
  - category: meetings
    content: El juliol de 2009, tingué lloc a Gran Canaria, Espanya, la primera [Desktop
      Summit](http://www.grancanariadesktopsummit.org/) («Cimera d'escriptori»), un
      congrés conjunt de les comunitats KDE i Gnome. L'[Akademy 2009](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009)
      se celebrà amb aquest esdeveniment.
    image:
    - caption: Foto de grup de la DS 2009
      src: /images/akademy-2009.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/
    title: Desktop Summit («Cimera d'escriptori») de Gran Canaria
  - category: kde-events
    content: La comunitat [arribà a la marca d'1 milió de «commits»](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository).
      Des de 500.000 el gener de 2006 i 750.000 el desembre de 2007, i només 19 mesos
      després, les col·laboracions arribaren a la marca d'1 milió. L'increment d'aquestes
      col·laboracions va coincidir amb el llançament de l'innovador KDE 4.
    image:
    - caption: Col·laboradors actius en cada moment
      src: /images/active-contributors.png
      url: https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png
    title: 1 milió de «commits»
  - category: meetings
    content: El setembre de 2009, tingué lloc a Randa, als Alps suïssos, [la primera](https://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds)
      d'una sèrie d'esdeveniments coneguts com [reunions de Randa](https://community.kde.org/Sprints/Randa).
      L'esdeveniment aplegà junts diversos esprints de diferents projectes de la comunitat.
      Des de llavors, les reunions de Randa tenen lloc anualment.
    image:
    - caption: Foto de grup de la primera RM
      src: /images/randameetings.jpg
      url: https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg
    title: Primera reunió a Randa
  - category: kde-events
    content: El novembre de 2009, la comunitat [anuncià](https://dot.kde.org/2009/11/24/repositioning-kde-brand)
      canvis a la seva marca. El nom «K Desktop Environment» havia esdevingut ambigu
      i obsolet i se substitueix per «KDE». El nom «KDE» ja no es només per a referir-se
      a l'entorn d'escriptori, sinó que ara representa tant la comunitat com el projecte
      paraigües sustentat per aquesta comunitat.
    image:
    - caption: Gràfic de la marca
      src: /images/brandmap.png
      url: https://dot.kde.org/sites/dot.kde.org/files/brandmap.png
    title: Reconstrucció de la marca
  - category: kde-events
    content: Des de la [versió 4.3.4](https://www.kde.org/announcements/announce-4.3.4.php),
      els anuncis de KDE comencen a al·ludir a tot el conjunt de productes com a «KDE
      Software Compilation» (KDE SC) (Compilació de programari de KDE). Actualment,
      aquesta tendència s'ha abandonat.
    image:
    - caption: Mapa de la marca
      src: /images/kde_brand_map.png
      url: https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png
    title: Compilació de programari KDE
  year: 2009
- entries:
  - category: meetings
    content: L'abril de 2010, tingué lloc [la primera reunió](http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf)
      dels col·laboradors de KDE de Brasil. Aquest esdeveniment es va celebrar a Salvador,
      Bahia, i fou l'única edició de l'Akademy brasilera. Des de 2012, l'esdeveniment
      s'ha ampliat a una reunió de tots els col·laboradors d'Amèrica Llatina.
    image:
    - caption: Foto de grup de l'Akademy-Br
      src: /images/akademy-br.jpeg
      url: https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg
    title: Akademy-Br
  - category: kde-events
    content: El juny de 2010, la KDE e.V. anuncià el programa d'afiliació de patrocini
      [«Join the Game»](https://jointhegame.kde.org/), amb l'objectiu de fomentar
      l'ajuda financera a la comunitat. En participar en el programa s'esdevé membre
      de la KDE e.V., col·laborant amb un import anual i podent participar en les
      reunions anuals de l'organització.
    image:
    - caption: Logotip de JtG
      src: /images/join-the-game.png
      url: https://kde.org/announcements/4/4.9.0/images/join-the-game.png
    title: Join the Game
  - category: releases
    content: 'L''agost de 2010, la comunitat [anuncià la versió 4.5](https://www.kde.org/announcements/4.5/)
      dels seus productes: Plataforma de desenvolupament, Aplicacions i Espais de
      treball del Plasma. Cada un d''ells va començar a tenir un anunci de llançament
      separat. El més destacat d''aquesta versió fou la interfície del Plasma per
      a ordinadors ultraportàtils, anunciat a la versió 4.4.'
    image:
    - caption: Captura de pantalla del Plasma Netbook
      src: /images/plasma-netbook.png
      url: https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png
    title: KDE SC 4.5
  - category: releases
    content: El desembre de 2010, la comunitat [anuncià](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite)
      la [Calligra Suite](https://www.calligra.org/), una divisió des del paquet ofimàtic
      KOffice. El KOffice es va interrompre el 2011.
    image:
    - caption: Logotip de la Calligra Suite
      src: /images/banner_calligra.png
      url: /images/banner_calligra.png
    title: Calligra Suite
  year: 2010
- entries:
  - category: meetings
    content: El març de 2011, tingué lloc a Bengaluru el [primer congrés](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india)
      de les comunitats KDE i Qt a l'Índia. Des de llavors, l'esdeveniment ha tingut
      lloc anualment.
    image:
    - caption: Foto de grup de la Conf India
      src: /images/conf_kde_india.jpg
      url: https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/
    title: Primer congrés de KDE India
  - category: meetings
    content: L'agost de 2011, tingué lloc a Berlin [un altre congrés conjunt](https://desktopsummit.org/)
      de les comunitats KDE i Gnome, Alemanya. Prop de 800 col·laboradors de tot el
      món es trobaren plegats per a compartir idees i col·laborar en diversos projectes
      de programari lliure.
    image:
    - caption: Foto de grup de la DS 2011
      src: /images/desktop-summit.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/
    title: Desktop Summit («Cimera d'escriptori») de 2011
  - category: releases
    content: La comunitat llançà la primera versió de la seva interfície per a dispositius
      mòbils, el [Plasma Active](https://www.kde.org/announcements/plasma-active-one/).
      Més tard, es va substituir pel Plasma Mobile.
    image:
    - caption: Captura de pantalla del Plasma Active 1
      src: /images/plasma_active.png
      url: https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png
    title: Plasma Active
  year: 2011
- entries:
  - category: meetings
    content: L'abril de 2012, tingué lloc la [primera reunió](https://lakademy.kde.org/lakademy12-en.html)
      dels col·laboradors de KDE a Amèrica Llatina, LaKademy. L'esdeveniment es va
      celebrar a Porto Alegre, Brasil. La [segona edició](https://br.kde.org/lakademy-2014)
      tingué lloc a São Paulo, i des de llavors ha estat un esdeveniment anual. Fina
      ara totes les edicions s'han celebrat a Brasil, a on hi ha el nombre més gran
      de col·laboradors de la comunitat llatinoamericana.
    image:
    - caption: Foto de grup de LaKademy 2012
      src: /images/lakademy.jpg
      url: https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg
    title: Primera LaKademy
  - category: kde-events
    content: 'Es va publicar el [KDE Manifesto](https://manifesto.kde.org/index.html),
      un document que presenta els beneficis i les obligacions d''un projecte KDE.
      També presenta els valors centrals que guien la comunitat: govern obert, programari
      lliure, inclusivitat, innovació, propietat comuna, i enfocament a l''usuari
      final.'
    image:
    - caption: Art del KDE Manifesto
      src: /images/tree.png
      url: https://manifesto.kde.org/images/tree.png
    title: KDE Manifesto
  - category: kde-events
    content: El desembre de 2012, la comunitat [llançà un concurs](https://dot.kde.org/2012/12/08/contest-create-konqi-krita)
      per a crear una mascota nova usant el Krita. El guanyador del concurs fou Tyson
      Tan, que va crear [aparences noves per al Konqi i la Katie](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237).
    image:
    - caption: Konqi redissenyat
      src: /images/mascot_konqi.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Konqi nou
  year: 2012
- entries:
  - category: kde-events
    content: El setembre de 2013, la comunitat [anuncià](https://dot.kde.org/2013/09/04/kde-release-structure-evolves)
      canvis al cicle de llançament dels seus productes. Cadascun d'ells, Espais de
      treball, Aplicacions, i Plataforma, ara tenen llançaments separats. El canvi
      fou el reflex de la reestructuració de les tecnologies KDE. Aquesta reestructuració
      produí la propera generació de productes de la comunitat, que es llançarien
      l'any següent.
    image:
    - caption: Gràfic de les tecnologies KDE separades
      src: /images/KDE5.png
      url: https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html
    title: Canvi del cicle de llançaments
  year: 2013
- entries:
  - category: releases
    content: '[Es va llançar](https://www.kde.org/announcements/kde-frameworks-5.0.php)
      la primera versió estable dels Frameworks 5 (KF5), hereva de la Plataforma KDE
      4. Aquesta generació nova de les biblioteques KDE basades en les Qt 5 van fer
      més modular el desenvolupament de la plataforma KDE i facilitaren el desenvolupament
      multiplataforma.'
    image:
    - caption: Evolució del desenvolupament de les tecnologies KDE
      src: /images/Evolution_KDE.png
      url: https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg
    title: Frameworks 5
  - category: releases
    content: '[Llançament](https://www.kde.org/announcements/plasma5.0/) de la primera
      versió estable del Plasma 5. Aquesta generació nova del Plasma té un tema nou,
      Brisa. Els canvis inclouen una migració a una pila gràfica nova plenament accelerada
      per maquinari centrada en l''escenògraf OpenGL(ES). Aquesta versió del Plasma
      usa com a base les Qt 5 i els Frameworks 5.'
    image:
    - caption: Captura de pantalla del Plasma 5
      src: /images/plasma5.png
      url: https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png
    title: Plasma 5
  - category: kde-events
    content: El desembre de 2014, el paquet de programari educatiu [GCompris s'uneix](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser)
      a l'[incubador de projectes de la comunitat KDE](https://community.kde.org/Incubator).
      Bruno Coudoin, que creà el [projecte](http://gcompris.net/index-en.html) el
      2000, va decidir reescriure'l en Qt Quick per a facilitar el seu ús en plataformes
      mòbils. Originalment estava escrit en GTK+.
    image:
    - caption: Logotip del GCompris
      src: /images/gcompris-logo.png
      url: /images/gcompris-logo.png
    title: El GCompris s'uneix a KDE
  - category: releases
    content: La comunitat [anuncià el Plasma Mobile](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform),
      una interfície per a telèfons intel·ligents que usa les tecnologies Qt, Frameworks
      5 i Plasma Shell.
    image:
    - caption: Foto del Plasma Mobile
      src: /images/plasma-mobile.jpg
      url: https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg
    title: Plasma Mobile
  year: 2014
- entries:
  - category: releases
    content: La [primera imatge en viu](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image)
      del Plasma executant-se en el Wayland es posà a disposició per a baixar-la.
      Des del 2011, la comunitat treballa per a implementar el Wayland en el KWin,
      el compositor del Plasma, i en el gestor de finestres.
    image:
    - caption: KWin en el Wayland
      src: /images/plasma-wayland.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png
    title: Plasma en el Wayland
  - category: releases
    content: 'La versió 5.5 [s''anuncià](https://www.kde.org/announcements/plasma-5.5.0.php)
      amb diverses funcionalitats noves: icones noves afegides al tema Brisa, implementació
      de l''OpenGL ES en el KWin, progrés per a implementar el Wayland, un tipus de
      lletra predeterminat nou (Noto), un disseny nou.'
    image:
    - caption: Captura de pantalla del Plasma 5.5
      src: /images/plasma5-5.png
      url: https://kde.org/announcements/plasma/5/5.5.0/discover.png
    title: Plasma 5.5
  year: 2015
- entries:
  - category: kde-events
    content: La comunitat [anuncià](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon)
      la inclusió d'un altre projecte a l'incubador, el [KDE Neon](https://neon.kde.org/),
      basat en l'Ubuntu. Els desenvolupadors, provadors, artistes, traductors i els
      primers adoptants poden obtenir el codi fresc des del «git» tan aviat el publica
      la comunitat KDE.
    image:
    - caption: Captura de pantalla del Neon 5.6
      src: /images/neon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/installer.png
    title: KDE Neon
  - category: kde-events
    content: L'[Akademy 2016](https://akademy.kde.org/2016) tingué lloc com a part
      de la [QtCon](http://qtcon.org/) el setembre de 2016 a Berlin, Alemanya. L'esdeveniment
      aplegà els comunitats Qt, [FSFE](https://fsfe.org/), [VideoLAN](http://www.videolan.org/)
      i KDE. Se celebrà els 20 anys de KDE, els 20 anys del VLC, i els 15 anys de
      la FSFE.
    image:
    - caption: Rètol de la QtCon
      src: /images/QtCon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png
    title: Akademy 2016 part de la QtCon
  - category: releases
    content: '[Es llançà el Kirigami](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui),
      un conjunt de components QML per a desenvolupar aplicacions basades en les Qt
      per a dispositius mòbils o d''escriptori.'
    image:
    - caption: Logotip del Kirigami
      src: /images/kirigami.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kirigami.png
    title: Kirigami UI
  - category: kde-events
    content: 'A principis de 2016, com a resultat d''una enquesta i de debats oberts
      entre els membres de la comunitat, [KDE va publicar un document que introduïa
      la seva visió pel futur](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future).
      Aquesta visió representa els valors que els seus membres consideren més importants:
      «Un món en que tothom té control sobre la seva vida digital i gaudeix de llibertat
      i privadesa». La idea en definir aquesta visió era deixar clar quines són les
      motivacions principals que guien a la comunitat.'
    image:
    - caption: ''
      src: /images/vision_collage.jpg
    title: KDE presenta la seva visió pel futur
  - category: kde-events
    content: Per tal de formalitzar la cooperació entre la comunitat i les organitzacions
      quan han estat aliades, [KDE e.V. anuncià el Comitè consultiu](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board)
      («Advisory Board»). A través del Comitè consultiu, les organitzacions poden
      proporcionar comentaris de les activitats i decisions de la comunitat, participar
      en les reunions normals de KDE e.V., i assistir a les Akademy i als esprints
      de la comunitat.
    image:
    - caption: ''
      src: /images/advisory_board.jpg
    title: KDE anuncià el Comitè consultiu
  - category: kde-events
    content: El 14 d'octubre, KDE celebrà el 20è aniversari. El projecte que començà
      com un entorn d'escriptori per a sistemes Unix, avui és una comunitat que incuba
      idees i projectes que van més enllà de les tecnologies d'escriptori. Per a celebrar
      el seu aniversari la comunitat [publicà un llibre](https://20years.kde.org/book/)
      escrit pels seus col·laboradors. [També van haver-hi festes](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary)
      celebrades a vuit països.
    image:
    - caption: Art dels 20 anys de KDE fet per l'Elias Silveira
      src: /images/kde20_anos.png
    title: KDE celebrà els 20 anys
  year: 2016
- entries:
  - category: kde-events
    content: En associació amb un comerciant espanyol de portàtils [la comunitat anuncià
      el llançament del KDE Slimbook](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fan),
      un ultraportàtil que porta preinstal·lat el Plasma KDE i les aplicacions KDE.
      El portàtil ofereix una versió preinstal·lada del KDE Neon i es [pot comprar
      des del lloc web del comerciant](https://slimbook.es/en/store/slimbook-kde).
    image:
    - caption: KDE Slimbook
      src: /images/kde_slimbook.jpg
    title: S'anuncia el KDE Slimbook
  - category: kde-events
    content: Inspirada en la QtCon 2016, que tingué lloc a Berlin i aplegà les comunitats
      KDE, VLC, Qt i FSFE, la [comunitat KDE al Brasil allotjà la QtCon Brasil en
      2017](https://br.qtcon.org/2017/). L'esdeveniment se celebrà a São Paulo i aplegà
      experts de Qt del Brasil i de l'estranger en dos dies de xerrades i un dia de
      formació.
    image:
    - caption: ''
      src: /images/qtconbr.png
    title: S'anuncia la QtCon Brasil
  - category: kde-events
    content: '[KDE i Purism s''han associat per a adaptar el Plasma Mobile al telèfon
      intel·ligent Librem 5](https://www.kde.org/announcements/kde-purism-librem5.php),
      fabricat per l''empresa americana. El Librem 5 és un telèfon enfocat en mantenir
      la privadesa i la seguretat de les comunicacions de l''usuari. El projecte d''adaptació
      està actualment en curs i aviat tindrem el primer telèfon del món completament
      controlat per l''usuari i executant el Plasma Mobile.'
    image:
    - caption: ''
      src: /images/purism.png
    title: Associació de KDE i Purism
  - category: kde-events
    content: '[KDE va definir els seus objectius per als quatre anys següents](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond).
      Com a part d''un esforç compromès pels seus membres des de 2015, la comunitat
      ha definit tres objectius principals per als anys vinents: millorar la usabilitat
      i la productivitat el seu programari, assegurar que el seu programari ajuda
      a mantenir la privadesa dels usuaris i facilitar l''aportació i la integració
      dels col·laboradors nous.'
    image:
    - caption: ''
      src: /images/kde_goals.jpg
    title: KDE defineix els objectius
  year: 2017
floss: Esdeveniments FLOSS
kde: Esdeveniments KDE
meetings: Reunions
releases: Llançaments
start: Inici
---
