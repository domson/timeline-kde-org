---
all: Alla
events:
- entries:
  - category: floss-events
    content: Under 1969 börjar [Ken Thompson](https://en.wikipedia.org/wiki/Ken_Thompson)
      och [Dennis Ritchie](https://en.wikipedia.org/wiki/Dennis_Ritchie) arbeta på
      [UNIX](http://www.unix.org/what_is_unix/history_timeline.html). Ursprungligen
      skrivet i assembler, skrevs det snart om i [C](https://en.wikipedia.org/wiki/C_(programming_language)),
      ett språk skapat av Ritchie som ansågs vara högnivå.
    image:
    - caption: Thompson & Ritchie
      src: /images/Thompson_Ritchie.jpg
      url: https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/
    title: UNIX föds
  year: 1969
- entries:
  - category: floss-events
    content: Under 1979 påbörjar [Bjarne Stroustrup](http://www.stroustrup.com/bio.html)
      utveckla "C med klasser", som senare skulle bli [C++](http://www.softwarepreservation.org/projects/c_plus_plus/).
      Enligt hans åsikt, var språket det enda från den tiden som gjorde det möjligt
      att skriva program som samtidigt var effektiva och eleganta.
    image:
    - caption: Bjarne Stroustrup
      src: /images/BjarneStroustrup.jpg
      url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup#/media/File:BjarneStroustrup.jpg
    title: C++ skapas
  year: 1979
- entries:
  - category: floss-events
    content: Under 1984 börjar [Richard Stallman](https://stallman.org/biographies.html#serious)
      utveckla [GNU](https://www.gnu.org/gnu/about-gnu.html) (GNU is Not Unix), ett
      fullständigt fritt operativsystem baserat på Unix, som var proprietärt.
    image:
    - caption: Richard Stallman
      src: /images/richard_stallman.jpg
      url: https://phoneia.com/en/30-years-of-the-gnu-manifesto-written-by-richard-stallman/
    title: Början av fri programvara
  year: 1984
- entries:
  - category: floss-events
    content: Under 1991 skapar [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds)
      [Linux kärnan](https://en.wikipedia.org/wiki/Linux_kernel) baserad på [MINIX](http://www.minix3.org/),
      en version av Unix skriven av [Andrew Tanenbaum](http://www.cs.vu.nl/~ast/home/faq.html).
      Framväxten av Linux har revolutionerat den fria programvarans historia och hjälpt
      till att popularisera den. Se infografiken [25 år med utveckling av Linux kärnan](https://web.archive.org/web/20190628003733im_/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-2016.jpg?itok=DqVqiplt).
    image:
    - caption: Linus Torvalds
      src: /images/linus.jpg
      url: https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html
    title: Linux-kärnan
  year: 1991
- entries:
  - category: floss-events
    content: Under 1993 börjar de första fria distributionerna dyka upp, baserade
      på [GNU och Linux](http://www.aboutlinux.info/2005/11/complete-concise-history-of-gnulinux.html).
      En [GNU/Linux distribution](http://futurist.se/gldt/) utgörs vanligtvis av Linux-kärnan,
      GNU-verktyg och bibliotek, och mer en samling program.
    image:
    - caption: GNU och Tux
      src: /images/gnulinuxLogo.png
      url: /images/gnulinuxLogo.png
    title: De första distributionerna föds
  year: 1993
- entries:
  - category: floss-events
    content: Under 1995 skapar det norska företaget Troll Tech ramverket [Qt](https://wiki.qt.io/About_Qt)
      för flera plattformar, som gjorde det möjligt att skapa KDE nästa år. Qt blev
      grunden för KDE:s huvudteknologier under de här 20 åren. Ta reda på mer om [Qt:s
      historia](https://wiki.qt.io/Qt_History).
    image:
    - caption: Qt logotyp
      src: /images/qt-logo.png
      url: https://doc.qt.io/qt-5/qtquickcontrols2-gallery-example.html
    title: Qt skapas
  year: 1995
- entries:
  - category: kde-events
    content: Under 1996 tillkännager [Matthias Ettrich](http://www.linuxjournal.com/article/6834)
      skapandet av Kool Desktop Environment (KDE), ett grafiskt gränssnitt för Unix-system,
      byggt med Qt och C ++, konstruerat för slutanvändare. Namnet "KDE" var en ordvits
      baserat på den grafiska miljön [CDE](https://en.wikipedia.org/wiki/Common_Desktop_Environment),
      som var proprietärt vid tillfället. Läs [det ursprungliga tillkännagivandet](https://www.kde.org/announcements/announcement.php)
      av KDE-projektet.
    image:
    - caption: Matthias Ettrich
      src: /images/Matthias.jpg
      url: https://www.linuxjournal.com/article/6834
    title: KDE tillkännages
  year: 1996
- entries:
  - category: meetings
    content: Under 1997 träffas omkring 15 KDE-utvecklare i Arnsberg, Germany, för
      att arbeta på projektet och diskutera dess framtid. Evenemanget blev känt som
      [KDE One](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting)).
    image:
    - caption: Cornelius Schumachers arkiv
      src: /images/kde-one-arnsberg.png
    title: KDE One konferens
  - category: releases
    content: Beta version 1 av KDE [ges ut](https://www.kde.org/announcements/beta1announce.php)
      exakt 12 månader efter projektet tillkännagavs. Utgivningstexten betonar att
      KDE inte bara är en fönsterhanterare, utan en integrerad miljö där fönsterhanteraren
      bara är en annan del.
    image:
    - caption: Skärmbild av KDE Beta 1
      src: /images/KDEBeta1.gif
      url: /images/KDEBeta1.gif
    title: KDE Beta 1
  - category: kde-events
    content: Under 1997 grundas [KDE e.V.](https://ev.kde.org/), den ideella organisation
      som representerar KDE-gemenskapen ekonomiskt och juridiskt, i Tübingen Tyskland.
    image:
    - caption: KDE e.V. logotyp
      src: /images/ev_large.png
      url: https://ev.kde.org/images/ev_large.png
    title: KDE e.V. grundas
  year: 1997
- entries:
  - category: kde-events
    content: Den grundläggande överenskommelsen för [KDE Free Qt-stiftelsen](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php)
      skrivs under av KDE e.V. och Trolltech, dåvarande ägare av Qt. Stiftelsen [säkerställer
      permanent tillgänglighet](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update)
      av Qt som fri programvara.
    image:
    - caption: Konqi med Qt i sitt hjärta
      src: /images/kde-qt.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: KDE Free Qt-stiftelsen skapas
  - category: releases
    content: KDE ger ut [den första stabila versionen](https://www.kde.org/announcements/announce-1.0.php)
      av den grafiska miljön 1998, med höjdpunkter som ett utvecklingsramverk, KOM/OpenParts,
      och en förhandsversion av dess kontorssvit. Se [Skärmbilder av KDE 1.x](https://czechia.kde.org/screenshots/kde1shots.php).
    image:
    - caption: KDE 1
      src: /images/KDE1.png
      url: https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png
    title: KDE 1 ges ut
  year: 1998
- entries:
  - category: kde-events
    content: April 1999 tillkännages en drake som ny animerad medhjälpare till KDE:s
      hjälpcentral. Den visade sig vara så charmig att den ersatte den tidigare projektmaskoten,
      Kandalf, från och med version 3.x. Se [Skärmbilder av KDE 2](https://czechia.kde.org/screenshots/images/large/kde2b3_5.png)
      som visar Konqi och Kandalf.
    image:
    - caption: Konqi
      src: /images/Konqi.png
      url: https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png
    title: Konqi
  - category: meetings
    content: Oktober 1999 äger det andra mötet mellan KDE-utvecklare rum i Erlangen,
      Tyskland. Läs [rapporten](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting))
      om konferensen KDE Two.
    image:
    - caption: Gruppfoto (Cornelius Schumachers arkiv)
      src: /images/kde-two-erlangen.jpg
    title: KDE Two konferens
  year: 1999
- entries:
  - category: releases
    content: Från [beta 1 versionen av KDE 2](https://www.kde.org/announcements/announce-1.90.php)
      är det möjligt att märka en ändring av projektnamnet. Utgåvan som tidigare kallade
      projektet "K-skrivbordsmiljön", börjande kalla det bara "KDE-skrivbordet".
    image:
    - caption: KDE 2 logotyp
      src: /images/KDE2logo.png
      url: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png
    title: KDE-skrivbord
  - category: meetings
    content: Juli 2000, äger det tredje mötet (beta) av KDE-utvecklare rum i Trysil,
      Norge. [Ta reda på](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting))
      vad som gjordes under konferensen.
    image:
    - caption: Cornelius Schumachers arkiv
      src: /images/kde-three-beta-meeting.jpg
    title: KDE Three Beta konferens
  - category: releases
    content: KDE [ger ut](https://kde.org/announcements/1-2-3/2.0/) sin andra version,
      vilken har som huvudnyhet [Konqueror](https://konqueror.org/) webbläsare och
      filhanterare, samt kontorssviten [Koffice](https://en.wikipedia.org/wiki/KOffice).
      KDE:s kod skrevs nästan om helt och hållet för den andra versionen. Se [Skärmbilder
      av KDE 2.0](https://czechia.kde.org/screenshots/kde2shots.php).
    image:
    - caption: KDE 2
      src: /images/kde2.png
      url: https://czechia.kde.org/screenshots/images/large/kde2b3_6.png
    title: KDE 2 ges ut
  year: 2000
- entries:
  - category: releases
    content: Från [utgivningsmeddelandet](https://www.kde.org/announcements/announce-2.1.2.php)
      för version 2.1.2 har namngivningen också ändrats. Meddelandet börjar referera
      till KDE som "KDE-projektet".
    image:
    - caption: KDE 2.1 startskärm
      src: /images/splashscreen-2.1.png
      url: https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png
    title: KDE-projektet
  - category: kde-events
    content: Mars 2001 tillkännages att gemenskapens kvinnogrupp har skapats. [KDE
      Women](https://community.kde.org/KDE_Women) syfte är att hjälpa till att öka
      antal kvinnor i fri programvara, i synnerhet KDE. Titta på videon ["Highlights
      of KDE Women"](https://www.youtube.com/watch?v=HTwQ-oGTmGA) från Akademy 2010.
    image:
    - caption: Katie, Konqis flickvän
      src: /images/katie.png
      url: https://www.kde.org/stuff/clipart/katie-221x223.jpg
    title: KDE Women
  year: 2001
- entries:
  - category: meetings
    content: Mars 2002 samlas ungefär 25 utvecklare för det [tredje KDE-mötet](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting))
      i Nuremberg Tyskland. KDE 3 skulle just ges ut och koden i KDE 2 behövde konverteras
      till det nya biblioteket Qt 3.
    image:
    - caption: Gruppfoto av KDE Three
      src: /images/ThreeMeeting.jpg
      url: https://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg
    title: KDE Three möte
  - category: releases
    content: KDE ger ut sin [tredje version](https://kde.org/announcements/1-2-3/3.0/),
      som uppvisar som viktiga nya tillägg utskriftsramverket, KDEPrint, översättning
      av projektet till 50 språk, och ett paket med utbildningsprogram, underhållna
      av KDE Edutainment projektet. Se [Skärmbilder av KDE 3.0](https://czechia.kde.org/screenshots/kde300shots.php).
    image:
    - caption: KDE 3
      src: /images/kde3.jpg
      url: https://czechia.kde.org/screenshots/images/1152x864/kde300-snapshot2-1152x864.jpg
    title: KDE 3
  - category: kde-events
    content: Augusti 2002 hålls ett [möte med styrelsemedlemmarna i KDE e.V.](https://ev.kde.org/reports/2002.php)
      som var väsentligt för att upprätta hur organisationen ska fungera. På mötet
      bestämdes bland annat and varumärket "KDE" skulle registreras och att nya medlemmar
      skulle bjudas in och stödjas av två aktiva medlemmar i e.V.
    image:
    - caption: Gruppfoto (Cornelius Schumachers arkiv)
      src: /images/kde-ev-meeting.jpg
    title: KDE e.V. möte
  year: 2002
- entries:
  - category: releases
    content: I [version 3.1](https://kde.org/announcements/1-2-3/3.1/) presenterar
      gemenskapen KDE med ett nytt utseende, ett nytt tema för grafiska komponenter,
      kallat Keramik, och Crystal som standardtema för ikonerna. Se [Guide till nya
      funktioner i KDE 3.1](https://www.kde.org/info/3.1/feature_guide_1.html).
    image:
    - caption: KDE 3.1
      src: /images/kde31.png
      url: https://czechia.kde.org/screenshots/images/3.1/fullsize/8.png
    title: KDE 3.1
  - category: meetings
    content: Augusti 2003 samlas omkring 100 bidragsgivare till KDE från olika länder
      i ett slott i Tjeckien. Evenemanget kallades [Kastle](https://akademy.kde.org/2003)
      och var föregångaren till Akademy, evenemanget som skulle bli gemenskapens internationella
      årliga möte.
    image:
    - caption: Gruppfoto av Kastle
      src: /images/Kastle.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg
    title: Kastle
  year: 2003
- entries:
  - category: meetings
    content: Augusti 2004, ägde [gemenskapens första internationella möte](https://conference2004.kde.org/)
      rum. Evenemanget hölls i Ludwigsburg Tyskland, och inledde en serie internationella
      evenemang kallade ["Akademy"](https://akademy.kde.org/) som äger rum varje år
      sedan dess. Evenemanget fick sitt namn på grund av att det ägde rum i stadens
      filmskola "Filmakademie". Se  [gruppfoton](https://devel-home.kde.org/~duffus/akademy/)
      från alla Akademy-möten.
    image:
    - caption: Gruppfoto av Akademy 2004
      src: /images/akademy-2004.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg
    title: Akademy 2004
  year: 2004
- entries:
  - category: releases
    content: '[KDE 3.5 ges ut](https://www.kde.org/announcements/announce-3.5.php).
      Versionen introducerade flera nya funktioner, bland dem,SuperKaramba, ett verktyg
      som gjorde det möjligt att anpassa skrivbordet med "miniprogram", Amarok och
      Kaffeine spelarna. och mediabrännaren K3B. Se [KDE 3.5: En visuell guide till
      nya funktioner](https://www.kde.org/announcements/visualguide-3.5.php).'
    image:
    - caption: KDE 3.5
      src: /images/kde35.png
      url: https://czechia.kde.org/screenshots/images/3.5/35-superkaramba.png
    title: KDE 3.5
  year: 2005
- entries:
  - category: meetings
    content: Mars 2006 ägde det [första mötet](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona)
      med spanska KDE-bidragsgivare rum i Barcelona. Sedan dess har Akademy-Es blivit
      ett årligt evenemang. Ta reda på mer om de [spanska KDE-bidragsgivarnas grupp](https://www.kde-espana.org/).
    image:
    - caption: Foto från första Akademy-es
      src: /images/akademy-es.jpg
      url: https://i.imgur.com/Le0ZAIf.jpg
    title: Första Akademy-Es
  - category: meetings
    content: Juli 2006 samlades utvecklarna av KDE:s kärnbibliotek i Trysill Norge
      till [mötet KDE Four Core](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core).
      Evenemanget var en sorts uppföljare till konferensen KDE Beta Three och mötet
      KDE Three, och utvecklarna arbetade där med utveckling av KDE 4 och stabilisering
      av en del kärnbibliotek i projektet.
    image:
    - caption: Gruppfoto (Cornelius Schumachers arkiv)
      src: /images/kde-four-core-meeting.jpg
    title: KDE Four Core möte
  year: 2006
- entries:
  - category: meetings
    content: Mars 2007 träffas flera bidragsgivare till KDE och  [Gnome](https://www.gnome.org/)
      i A Coruña Spanien, ett evenemang som försökte etablera ett samarbete mellan
      de två projekten. Evenemanget blev känt som [Guademy](https://dot.kde.org/2007/03/28/guademy-2007-event-report),
      en blandning av [Guadec](https://wiki.gnome.org/GUADEC), namnet givet till Gnome
      evenemang, med Akademy, KDE:s evenemangsnamn.
    image:
    - caption: Gruppfoto av Guademy
      src: /images/Guademy.jpg
      url: https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg
    title: Guademy
  - category: releases
    content: 'Maj 2007 tillkännages [alfa 1 versionen av KDE 4](https://www.kde.org/announcements/announce-4.0-alpha1.php),
      med kodnamn "Knut". Tillkännagivandet visade ett nytt skrivbord, med ett nytt
      tema, Oxygen, nya program som Okular och Dolphin, och ett nytt skrivbordsskal,
      Plasma. Se [KDE 4.0 alfa 1: En visuell guide till nya funktioner](https://www.kde.org/announcements/visualguide-4.0-alpha1.php).'
    image:
    - caption: KDE 4 Alfa 1
      src: /images/kde4alpha.png
      url: https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png
    title: KDE 4 Alfa 1
  - category: releases
    content: Oktober 2007 tillkännager KDE [utgivningskandidaten](https://www.kde.org/announcements/announce-4.0-platform-rc1.php)
      av dess utvecklingsplattform bestående av grundbibliotek och verktyg för att
      utveckla KDE-program.
    image:
    - caption: Konqi utvecklare
      src: /images/konqi-dev.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: KDE 4 utvecklingsplattform
  year: 2007
- entries:
  - category: releases
    content: 'Under 2008 tillkännager gemenskapen [det revolutionerade KDE 4](https://kde.org/announcements/4/4.0/).
      Förutom den visuella inverkan av det nya standardtemat, Oxygen, och det nya
      skrivbordsgränssnittet, Plasma, innoverade KDE 4 också genom att presentera
      följande program: PDF-läsaren Okular, filhanteraren Dolphin, samt Kwin med stöd
      för grafiska effekter. Se [KDE 4.0 visuell guide](https://kde.org/announcements/4/4.0/guide/).'
    image:
    - caption: KDE 4.0
      src: /images/kde4.jpg
      url: https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png
    title: KDE 4
  - category: meetings
    content: Från [tillkännagivandet av version 4.1](https://www.kde.org/announcements/4.1/)
      fanns redan en tendens till att hänvisa till KDE som en "gemenskap" och inte
      bara som ett "projekt". Ändringen erkändes och bekräftades i ommärkningen följande
      år.
    image:
    - caption: Konqis gemenskap
      src: /images/mascotes.png
      url: https://cibermundi.files.wordpress.com/2016/07/mascotes.png
    title: KDE-gemenskapen
  year: 2008
- entries:
  - category: meetings
    content: Januari 2009 äger [första upplagan av KDE-lägret](https://techbase.kde.org/Events/CampKDE/2009)
      rum i Negril Jamaica. Det var det första KDE evenemanget i Amerika. Därefter
      kom två ytterligare konferenser baserade i USA, [2010 i San Diego](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010),
      och en annan [2011 i San Francisco](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience).
    image:
    - caption: Gruppfoto 2009
      src: /images/campkde.jpg
      url: https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg
    title: Första KDE-lägret
  - category: meetings
    content: Juli 2009 äger det första [skrivbordstoppmötet](http://www.grancanariadesktopsummit.org/)
      rum i Gran Canaria Spanien, en gemensam konferens med KDE- och Gnome-gemenskaperna.
      [Akademy 2009](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009)
      hölls vid detta evenemang.
    image:
    - caption: Gruppfoto av DS 2009
      src: /images/akademy-2009.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/
    title: Gran Canaria skrivbordstoppmöte
  - category: kde-events
    content: Gemenskapen [når nivån 1 miljon incheckningar](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository).
      Från 500 000 i januari 2006 och 750 000 i december 2007, nådde bidragen nivån
      1 miljon bara 19 månader senare. Ökningen av bidragen sammanfaller med lanseringen
      av det innovativa KDE 4.
    image:
    - caption: Aktiva bidragsgivare vid tiden
      src: /images/active-contributors.png
      url: https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png
    title: 1 miljon incheckningar
  - category: meetings
    content: September 2009 ägde [det första](https://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds)
      av en serie evenemang kända som [Randa-möten](https://community.kde.org/Sprints/Randa)
      rum i Randa, i Schweiziska Alperna. Evenemanget sammanförde flera olika spurter
      i diverse gemenskapsprojekt. Sedan dess har Randa-möten skett varje år.
    image:
    - caption: Gruppfoto från första RM
      src: /images/randameetings.jpg
      url: https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg
    title: Första Randa-mötet
  - category: kde-events
    content: November 2009 [tillkännagav](https://dot.kde.org/2009/11/24/repositioning-kde-brand)
      gemenskapen ändringar av sitt varumärke. Name "K-skrivbordsmiljön" hade blivit
      tvetydigt och föråldrat och ersätts av "KDE". Namnet "KDE" betyder inte längre
      bara skrivbordsmiljön, utan representerar nu både gemenskapen och projektparaplyet
      som stöds av gemenskapen.
    image:
    - caption: Märkesdiagram
      src: /images/brandmap.png
      url: https://dot.kde.org/sites/dot.kde.org/files/brandmap.png
    title: Ommärkning
  - category: kde-events
    content: Från och med [version 4.3.4](https://www.kde.org/announcements/announce-4.3.4.php),
      börjar KDE:s tillkännagivanden kalla hela produktsviten 'KDE programvarusamling'
      (KDE SC). För närvarande har trenden övergivits.
    image:
    - caption: Märkeskarta
      src: /images/kde_brand_map.png
      url: https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png
    title: KDE programvarusamling
  year: 2009
- entries:
  - category: meetings
    content: April 2010 äger [det första mötet](http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf)
      av brasilianska KDE-bidragsgivare rum. Evenemanget hölls i Salvador Bahia, och
      var den enda brasilianska Akademy-upplagan. Från och med 2012 utökades evenemanget
      till ett möte för alla latinamerikanska bidragsgivare.
    image:
    - caption: Gruppfoto av Akademy-Br
      src: /images/akademy-br.jpeg
      url: https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg
    title: Akademy-Br
  - category: kde-events
    content: Juni 2010, tillkännager KDE e.V. stödmedlemskapsprogrammet ["Join the
      Game"](https://jointhegame.kde.org/), som har målet att uppmuntra till att stödja
      gemenskapen ekonomiskt. Genom att delta i programmet blir man medlem av KDE
      e.V., bidrar med ett årligt belopp och kan delta i organisationens årliga möten.
    image:
    - caption: JtG logotyp
      src: /images/join-the-game.png
      url: https://kde.org/announcements/4/4.9.0/images/join-the-game.png
    title: Join The Game
  - category: releases
    content: 'Augusti 2010 [tillkännager gemenskapen version 4.5](https://www.kde.org/announcements/4.5/)
      av dess produkter: utvecklingsplattform, program and Plasma arbetsrymder. Var
      och en av dem börjar ha separata utgivningsmeddelanden. En av versionens höjdpunkter
      var Plasma-gränssnittet för nätdatorer, tillkännagivet i version 4.4.'
    image:
    - caption: Plasma nätdator skärmbild
      src: /images/plasma-netbook.png
      url: https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png
    title: KDE SC 4.5
  - category: releases
    content: I december 2010, [tillkännagör](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite)
      gemenskapen [Calligra-sviten](https://www.calligra.org/), en avledning av KOffice-sviten.
      Utveckling av KOffice avbröts 2011.
    image:
    - caption: Calligra-svitens logotyp
      src: /images/banner_calligra.png
      url: /images/banner_calligra.png
    title: Calligra-sviten
  year: 2010
- entries:
  - category: meetings
    content: Mars 2011 ägde den [första konferensen](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india)
      med KDE- och Qt-gemenskapen i Indien rum i Bangalore. Sedan dess har evenemanget
      ägt rum varje år.
    image:
    - caption: Gruppfoto av konferens i Indien
      src: /images/conf_kde_india.jpg
      url: https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/
    title: Första KDE konferensen i Indien
  - category: meetings
    content: Augusti 2011 äger [ytterligare en gemensam konferens](https://desktopsummit.org/)
      mellan KDE- och Gnome-bidragsgivarna rum i Berlin Tyskland. Nästan 800 bidragsgivare
      från hela världen samlades för att dela idéer och samarbeta i diverse projekt
      med fri programvara.
    image:
    - caption: Gruppfoto av DS 2011
      src: /images/desktop-summit.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/
    title: Skrivbordstoppmöte 2011
  - category: releases
    content: Gemenskapen ger ut första versionen av gränssnittet för mobiler [Plasma
      Aktiv](https://www.kde.org/announcements/plasma-active-one/). Senare efterträddes
      det av Plasma Mobil.
    image:
    - caption: Plasma Aktiv 1 skärmbild
      src: /images/plasma_active.png
      url: https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png
    title: Plasma Aktiv
  year: 2011
- entries:
  - category: meetings
    content: April 2012 äger det [första mötet](https://lakademy.kde.org/lakademy12-en.html)
      av KDE-bidragsgivare i Latinamerika, LaKademy, rum. Evenemanget hölls i Porto
      Alegre Brasilien. Den [andra upplagan](https://br.kde.org/lakademy-2014) ägde
      rum under 2014 i São Paulo, och har sedan dess varit ett årligt evenemang. Hittills
      har alla upplagor hållits i Brasilien, där det största antalet bidragsgivare
      från Latinamerika är baserade.
    image:
    - caption: Gruppfoto av LaKademy 2012
      src: /images/lakademy.jpg
      url: https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg
    title: Första LaKademy
  - category: kde-events
    content: '[KDE:s manifest](https://manifesto.kde.org/index.html) ges ut, ett dokument
      som presenterar fördelarna och skyldigheterna för ett KDE-projekt. Det innehåller
      också kärnvärdena som styr gemenskapen: Öppen ledning, fri programvara, inkludering,
      innovation, gemensamt ägande och fokus på slutanvändaren.'
    image:
    - caption: Grafik för KDE:s manifest
      src: /images/tree.png
      url: https://manifesto.kde.org/images/tree.png
    title: KDE:s manifest
  - category: kde-events
    content: December 2012 [lanserar gemenskapen en tävling](https://dot.kde.org/2012/12/08/contest-create-konqi-krita)
      för att skapa en ny maskot med användning av Krita. Tävlingens vinnare var Tyson
      Tan, som skapade [nya utseenden för Konqi och Katie](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237).
    image:
    - caption: Konqi görs om
      src: /images/mascot_konqi.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Nya Konqi
  year: 2012
- entries:
  - category: kde-events
    content: September 2013 [tillkännager](https://dot.kde.org/2013/09/04/kde-release-structure-evolves)
      gemenskapen ändringar i utgivningscykeln av produkterna. Var och en av dem,
      arbetsrymder, program och plattform har nu separata utgåvor. Ändringen var redan
      en spegling av KDE-teknologiernas omstrukturering. Omstruktureringen ledde till
      nästa generation av gemenskapens produkter, som skulle ges ut följande år.
    image:
    - caption: Diagram av separata KDE-teknologier
      src: /images/KDE5.png
      url: https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html
    title: Ändring av utgivningscykeln
  year: 2013
- entries:
  - category: releases
    content: Den första stabila versionen av Ramverk 5 (KF5), efterföljaren till KDE
      plattform 4, [ges ut](https://www.kde.org/announcements/kde-frameworks-5.0.php).
      Den nya generationen av KDE-bibliotek baserade på Qt 5 har gjort KDE:s utvecklingsplattform
      mer modulär och främjat utveckling på flera plattformar.
    image:
    - caption: Evolution av KDE-teknologiernas utveckling
      src: /images/Evolution_KDE.png
      url: https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg
    title: Ramverk 5
  - category: releases
    content: '[Utgivning](https://www.kde.org/announcements/plasma5.0/) av den första
      stabila versionen av Plasma 5. Den nya generationen av Plasma har ett nytt tema,
      Breeze. Ändringar inkluderar en konvertering till en ny fullständigt hårdvaruaccelererad
      grafikrutiner centrerade runt en OpenGL(ES) scengraf. Den här versionen av Plasma
      använder Qt 5 och Ramverk 5 som bas.'
    image:
    - caption: Plasma 5 skärmbild
      src: /images/plasma5.png
      url: https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png
    title: Plasma 5
  - category: kde-events
    content: December 2014 går utbildningsprogramsviten [GCompris](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser)
      med i [KDE-gemenskapens projektinkubator](https://community.kde.org/Incubator).
      Bruno Coudoin, som skapat [projektet](http://gcompris.net/index-en.html) 2000,
      bestämde sig för att skriva om det i Qt Quick för att möjliggöra användning
      på mobiler. Det skrevs ursprungligen i GTK+.
    image:
    - caption: GCompris logotyp
      src: /images/gcompris-logo.png
      url: /images/gcompris-logo.png
    title: GCompris går med i KDE
  - category: releases
    content: Gemenskapen [tillkännager Plasma Mobil](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform),
      ett gränssnitt för smartmobiler som använder teknologierna Qt, Ramverk 5 och
      Plasma Skal.
    image:
    - caption: Plasma Mobil foto
      src: /images/plasma-mobile.jpg
      url: https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg
    title: Plasma Mobil
  year: 2014
- entries:
  - category: releases
    content: Den [första körbara avbilden](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image)
      av Plasma med Wayland görs tillgänglig för nerladdning. Sedan 2011 har gemenskapen
      arbetat på stöd för Wayland i Kwin, sammansättningsprogrammet i Plasma, och
      fönsterhanteraren.
    image:
    - caption: Kwin på Wayland
      src: /images/plasma-wayland.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png
    title: Plasma på Wayland
  - category: releases
    content: 'Version 5.5 [tillkännages](https://www.kde.org/announcements/plasma-5.5.0.php)
      med flera nya funktioner: nya ikoner tillagda i temat Breeze, stöd för OpenGL
      ES i Kwin, framsteg i stödet för Wayland, ett nytt standardteckensnitt (Noto),
      och en ny design.'
    image:
    - caption: Plasma 5.5 skärmbild
      src: /images/plasma5-5.png
      url: https://kde.org/announcements/plasma/5/5.5.0/discover.png
    title: Plasma 5.5
  year: 2015
- entries:
  - category: kde-events
    content: Gemenskapen [tillkännager](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon)
      att ett nytt program inkluderats i inkubatorn, [KDE Neon](https://neon.kde.org/),
      baserat på Ubuntu. Utvecklare, testare, grafiker, översättare och tidiga användare
      kan hämta aktuell kod från git när den checkas in av KDE-gemenskapen.
    image:
    - caption: Neon 5.6 skärmbild
      src: /images/neon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/installer.png
    title: KDE Neon
  - category: kde-events
    content: '[Akademy 2016](https://akademy.kde.org/2016) äger rum som en del av
      [QtCon](http://qtcon.org/) September 2016 i Berlin Tyskland. Evenemanget samlade
      Qt, [FSFE](https://fsfe.org/), [VideoLAN](http://www.videolan.org/) och KDE-gemenskaperna.
      Det firade 20 år av KDE, 20 år av VLC och 15 år av FSFE.'
    image:
    - caption: QtCon banderoll
      src: /images/QtCon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png
    title: Akademy 2016 en del av QtCon
  - category: releases
    content: '[Kirigami ges ut](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui),
      en uppsättning QML-komponenter för att utveckla program baserade på Qt för mobiler
      eller skrivbord.'
    image:
    - caption: Kirigami logotyp
      src: /images/kirigami.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kirigami.png
    title: Kirigami användargränssnitt
  - category: kde-events
    content: 'Tidigt 2016, som resultat av en enkät och öppen diskussion mellan gemenskapens
      medlemmar, [publicerar KDE ett dokument som beskriver dess framtidsvision](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future).
      Visionen representerar de värden som medlemmarna anser vara viktigast: "En värld
      där alla har kontroll över sina digitala liv och åtnjuter frihet och integritet."
      Idén med att definiera visionen var att klargöra vilka de viktigaste motiven
      som driver gemenskapen är.'
    image:
    - caption: ''
      src: /images/vision_collage.jpg
    title: KDE presenterar sin framtidsvision
  - category: kde-events
    content: För att formalisera samarbetet mellan gemenskapen och allierade organisationer,
      [tillkännager KDE e.V. rådgivande nämnden](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board).
      Via den rådgivande nämnden kan organisationer ge återkoppling om gemenskapens
      aktiviteter och beslut, delta i regelbundna möten med KDE e.V., samt delta i
      Akademy och gemenskapsspurter.
    image:
    - caption: ''
      src: /images/advisory_board.jpg
    title: KDE tillkännagör rådgivande nämnden
  - category: kde-events
    content: KDE firar sin 20:e födelsedag 14:e oktober. Projektet som startade som
      en skrivbordsmiljö för Unix-system, är idag en gemenskap som utvecklar idéer
      och projekt som går långt utanför skrivbordsteknologier. För att fira födelsedagen
      [publicerade gemenskapen en bok](https://20years.kde.org/book/) skriven av dess
      bidragsgivare. [Vi höll också fester](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary)
      i åtta länder.
    image:
    - caption: KDE 20 år av Elias Silveira
      src: /images/kde20_anos.png
    title: KDE firar 20 år
  year: 2016
- entries:
  - category: kde-events
    content: I samarbete med en spansk bärbar datoråterförsäljare tillkännagav [gemenskapen
      lanseringen av KDE Slimbook](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans),
      en bärbar dator som levereras med KDE Plasma och KDE program förinstallerade.
      Datorn erbjuder en förinstallerad version av KDE Neon och [kan köpas från återförsäljares
      webbplats](https://slimbook.es/en/store/slimbook-kde).
    image:
    - caption: KDE Slimbook
      src: /images/kde_slimbook.jpg
    title: KDE Slimbook tillkännages
  - category: kde-events
    content: Inspirerade av QtCon 2016, som ägde rum i Berlin och samlade KDE-, VLC-,
      Qt- och FSFE-gemenskaperna, var [KDE-gemenskapen i Brasilien värd för QtCon
      Brasilien 2017](https://br.qtcon.org/2017/). Evenemanget hölls i São Paulo och
      samlade Qt-experter från Brasilien och utlandet för två dagars föredrag och
      en dags utbildning.
    image:
    - caption: ''
      src: /images/qtconbr.png
    title: QtCon Brasilien tillkännages
  - category: kde-events
    content: '[KDE och Purism har ingått ett partnerskap för att anpassa Plasma Mobile
      till smartmobilen Librem 5](https://www.kde.org/announcements/kde-purism-librem5.php),
      tillverkad av det amerikanska företaget. Librem 5 är en telefon fokuserad på
      att bevara användarkommunikationens integritet och säkerhet. Anpassningsprojektet
      pågår och snart har vi den första telefonen i världen som helt kontrolleras
      av användaren och kör Plasma Mobil.'
    image:
    - caption: ''
      src: /images/purism.png
    title: Partnerskap mellan KDE och Purism
  - category: kde-events
    content: '[KDE bestämmer sina mål för de kommande fyra åren](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond).  Som
      en del av ett arbete som har genomförts av medlemmarna sedan 2015, har gemenskapen
      definierat tre huvudmål för de kommande åren: att förbättra användbarheten och
      produktiviteten av programvaran, att säkerställa att programvaran hjälper till
      att bevara användarnas integritet, samt att underlätta bidrag från och integration
      av nya medarbetare.'
    image:
    - caption: ''
      src: /images/kde_goals.jpg
    title: KDE definierar mål
  year: 2017
floss: FLOSS evenemang
kde: KDE evenemang
meetings: Möten
releases: Utgåvor
start: Start
---
